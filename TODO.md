# Project Name
Project Description

## TO-DOs
### Problem Description
- [ ] The problem is well described and it's clear what the problem the project solves (2p)

### CLoud
- [ ] The project is developed on the cloud OR uses localstack (or similar tool) OR the project is deployed to Kubernetes or similar container management platforms (2p)
- [ ] The project is developed on the cloud and IaC tools are used for provisioning the infrastructure  (4p)

### Experiment Tracking and Model Registry
- [ ] Both experiment tracking and model registry are used (4p)

### Workflow orchestration
- [ ] Fully deployed workflow (4p)

### Model deployment
- [ ] The model deployment code is containerized and could be deployed to cloud or special tools for model deployment are used (4p)

### Model monitoring
- [ ] Basic model monitoring that calculates and reports metrics (2p)
- [ ] Comprehensive model monitoring that sends alerts or runs a conditional workflow (e.g. retraining, generating debugging dashboard, switching to a different model) if the defined metrics threshold is violated (4p)

### Reproducibility
- [ ] Some instructions are there, but they are not complete OR instructions are clear and complete, the code works, but the data is missing (2p)
- [ ] Instructions are clear, it's easy to run the code, and it works. The versions for all the dependencies are specified. (4p)

### Best practices

- [ ] There are unit tests (1 point)
- [ ] There is an integration test (1 point)
- [ ] Linter and/or code formatter are used (1 point)
- [ ] There's a Makefile (1 point)
- [ ] There are pre-commit hooks (1 point)
- [ ] There's a CI/CD pipeline (2 points)

