#!/bin/bash

export PYTHONPATH=$(pwd)

# Run unit tests
pytest tests/

# Run integration test
python tests/integration_test.py
